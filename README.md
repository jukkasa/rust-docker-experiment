# rust-docker-experiment

Experimenting with simple Rust web service, Docker/Linux container building and Gitlab CI.

## Current status:
- Simple web server made with Actix-web
  - paths: / (index.html), /health (responds just 200 Ok), /hostname (returns OS hostname)
- Dockerfile that compiles the server application and creates a minimal container image of it (4.72MB).

## Todo:
- Add Gitlab CI
    - Create container image and publish it to image registry
- Add test cases?