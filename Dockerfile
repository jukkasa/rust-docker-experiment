FROM ekidd/rust-musl-builder:1.51.0 AS chef
USER root
RUN cargo install cargo-chef
WORKDIR /app

FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM chef AS builder
COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --target x86_64-unknown-linux-musl --recipe-path recipe.json
COPY . .
RUN cargo build --release --target x86_64-unknown-linux-musl

FROM alpine AS runtime
COPY static ./static
COPY --from=builder /app/target/x86_64-unknown-linux-musl/release/rust-docker-experiment .
USER 1000
EXPOSE 3000
CMD ["./rust-docker-experiment"]
