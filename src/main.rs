use actix_files as fs;
use actix_web::{App, HttpRequest, HttpResponse, HttpServer, Responder, get, middleware};
use dotenv::dotenv;
use std::env;
use hostname;

#[get("/health")]
async fn health() -> impl Responder {
    HttpResponse::Ok().finish()
}

#[get("/hostname")]
async fn get_hostname() -> impl Responder {
    let hn = hostname::get().unwrap_or_default().into_string().unwrap_or_default();
    HttpResponse::Ok().body(hn)
}

#[get("/env")]
async fn get_env() -> impl Responder {
    let mut resp = String::new();
    for (key, value) in std::env::vars() {
        resp.push_str(&format!("{}: {}\n", key, value));
    }    
    HttpResponse::Ok().body(resp)
}


#[get("/headers")]
async fn get_headers(req: HttpRequest) -> impl Responder {
    //let resp = format!("{:?}", req.headers());
    let mut resp = String::new();
    for (key, value) in req.headers().iter() {
        //resp.push_str(&format!("{:?}\n", header));
        resp.push_str(&format!("{}: {}\n", key, value.to_str().unwrap_or("WHAT! Some binary data???")));
    }
    HttpResponse::Ok().body(resp)
}

#[get("/dir")]
async fn get_dir() -> impl Responder {
    let mut resp = String::new();
    match std::fs::read_dir("/data") {
        Ok(readdir) => {
            for entry in readdir {
                resp.push_str(&format!("{:?}\n", entry));
            }
        },
        Err(error) => {
            resp = format!("{}", error);
        }
    }
    HttpResponse::Ok().body(resp)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    env_logger::init();

    let host = env::var("HOST").unwrap_or("0.0.0.0".to_string());
    let port = env::var("PORT").unwrap_or("3000".to_string());

    println!("Starting server at {}:{}", host, port);

    HttpServer::new(|| {
        App::new()
        //.wrap(middleware::Logger::default())
        .wrap(middleware::Logger::new("%{r}a \"%r\" %s %b \"%{Referer}i\" \"%{User-Agent}i\""))
        .service(health)
        .service(get_hostname)
        .service(get_env)
        .service(get_headers)
        .service(get_dir)
        .service(fs::Files::new("/", "./static/").index_file("index.html"))
    })
    .bind(format!("{}:{}", host, port))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    //use super::*;

    #[test]
    fn dummy_test() -> Result<(), String> {
        assert_eq!(1, 1);
        Ok(())
    }
}